from apscheduler.schedulers.background import BackgroundScheduler
from os import listdir
from os.path import isfile, join
from app import app



def scripts_list():
    """ Function for test purposes. """
    mypath = 'automizer/scripts/'
    onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]

    list = []
    if onlyfiles:
        for file in onlyfiles:
            if file != 'init.py':
                if file not in list:
                    list.append(file)
                if file in list and file not in onlyfiles:
                    list.count(file)
    print(f"{onlyfiles}")
    return list


# sched = BackgroundScheduler(daemon=True)
# sched.add_job(sensor, 'interval', seconds=10)
# sched.start()


if __name__ == "__main__":
    app.run()
