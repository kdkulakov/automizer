from flask import render_template
from app import app
from main import scripts_list
from flask import jsonify
from flask import request, redirect
import subprocess, os
import psutil
import netifaces


@app.route('/')
@app.route('/index')
def index():
    PROTO = netifaces.AF_INET  # We want only IPv4, for now at least

    # Get list of network interfaces
    # Note: Can't filter for 'lo' here because Windows lacks it.
    ifaces = netifaces.interfaces()

    # Get all addresses (of all kinds) for each interface
    if_addrs = [netifaces.ifaddresses(iface) for iface in ifaces]

    # Filter for the desired address type
    if_inet_addrs = [addr[PROTO] for addr in if_addrs if PROTO in addr]

    iface_addrs = [s['addr'] for a in if_inet_addrs for s in a if 'addr' in s]
    # Can filter for '127.0.0.1' here.

    return render_template("main_template.html",
        title = 'Scripts',
        files = scripts_list(),
        load_average = os.getloadavg(),
        used_memory = psutil.virtual_memory().percent,
        avaliable_memory = int(psutil.virtual_memory().available * 100 / psutil.virtual_memory().total),
        cpu_load = psutil.cpu_percent(),
        uptime = psutil.boot_time(),
        disk_usage = psutil.disk_usage(os.sep).percent,
        intrefaces = netifaces.interfaces(),
        list_net_addrs = iface_addrs
         )


@app.route('/script/', methods=["POST"])
def click():
    request_data = request.form.get('button')
    subprocess.call(['/usr/local/bin/python3', './scripts/' + request_data])
    return redirect('/', code=301)


@app.route('/load-average/', methods=["POST", "GET"])
def load_average():
    return jsonify(os.getloadavg())


@app.route('/free-memory/', methods=["POST", "GET"])
def free_memory():
    return jsonify(psutil.getloadavg)