#!/usr/local/bin/python3
import subprocess
from init import TERMINAL, TERMINAL_EX_arg


def main():
    subprocess.Popen([TERMINAL, TERMINAL_EX_arg, 'htop'])


if __name__ == "__main__":
    main()